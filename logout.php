<?php

session_start();

if (isset($_POST['logout'])) {
	$_SESSION = array();
	session_destroy();
	header('Location: index.php');
	exit;

}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Logout</title>
	</head>

	<body>
		<h1>Sign Out</h1>
		<form method="POST" action="">
			<p> Hello <?php echo $_SESSION['email']; ?> <p/>
				<br/>
			<input type="submit" value="logout" name="logout"/>

		</form>
	</body>
</html>